# Contracts for Slots Machines #

* All calls should use https
* We employ url encoded in the body (POST)  calls

## Bet contract sugestion ##



### URL call ###
```  
<domain>/v1/bet
```
Temporal URL for testing:

https://saracarrion.com/trivial/backend/bet.php

### Body of call ###

id: user id (our user id, not facebook id).

token: token for the call.

key: aditional control string.

project: id of the game (integer).


Example:

```
id:39403393
token:Z9df0a7dgS1ej1Em37grfa24o8RL
key:key
project:2001
Content-Type:application/x-www-form-urlencoded
```
### Response ###

```
{"reel1":"Goomba",
"reel2":"Life Mushroom",
"reel3":"Mario Hat",
"reel4":"Question Coin",
"reel5":"Goomba",
"status":true,
"prize":0,
"project":"2001"}
```

status: True for response ok, false for errors.

id: User id (our user id, not facebook id).

token: token for next call.

bet: Result of the bet.

project: Current project(game). 

### Curl example ###

```
curl -X POST \

  https://saracarrion.com/trivial/backend/bet.php \
  
  -H 'cache-control: no-cache' \
  
  -H 'content-type: application/x-www-form-urlencoded' \
  
  -H 'postman-token: bba0b107-2089-fe47-fb61-104b7621d0de' \
  
  -d 'id=39403393&degrees=300&token=Z9df0a7dgS1ej1Em37grfa24o8RL&key=key&project=2001&Content-Type=application%2Fx-www-form-urlencoded'
 ```