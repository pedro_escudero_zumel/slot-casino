"use strict";
const SPIN_START =  'START';
const SPIN_STOP = 'STOP';
const BET_DEFAULT = 100;
const REELS = 5;
const PAYLINES = 1;
var bet = BET_DEFAULT;
var fpsText;
var spinsheet = [];
var spin = [];
var stopButton = [];
var stopButtonAudio;
var spinButtonAudio;
var autoButton;
var spinButton;
var auto = false;
var canSpin = true;
var winAudio;
var game;
var money = 2000;
var moneyText;
var betText;
var prizeText;
var hitLine;
var xhr = new XMLHttpRequest();
var xhr_money = new XMLHttpRequest();
var SlotOneResult, SlotTwoResult, SlotThreeResult, SlotFourResult, SlotFiveResult;
var prize;
var header;
var buy, shareGame, lobby;
var prizeMoney;

function timingReel1 (){
    while (spin[0].currentFrame.name != SlotOneResult){
        console.log("delayed");
        spin[0].next();
    }
    spin[0].stop();
    stopButtonAudio.play();
    var frame = spin[0].currentFrame.index;
    if(frame % 4 === 1){
       spin[0].next(3);
    }else if(frame % 4 === 2){
       spin[0].next(2);
    }else if(frame % 4 === 3){
       spin[0].next(1);
    }
}

function timingReel2 (){
    while (spin[1].currentFrame.name != SlotTwoResult){
        console.log("delayed");
        spin[1].next();
    }
    spin[1].stop();
    stopButtonAudio.play();
    var frame = spin[1].currentFrame.index;
    if(frame % 4 === 1){
       spin[1].next(3);
    }else if(frame % 4 === 2){
       spin[1].next(2);
    }else if(frame % 4 === 3){
       spin[1].next(1);
    }
}

function timingReel3 (){
    while (spin[2].currentFrame.name != SlotThreeResult){
        console.log("delayed");
        spin[2].next();
    }
    spin[2].stop();
    stopButtonAudio.play();
    var frame = spin[2].currentFrame.index;
     if(frame % 4 === 1){
       spin[2].next(3);
    }else if(frame % 4 === 2){
       spin[2].next(2);
    }else if(frame % 4 === 3){
       spin[2].next(1);
    }
}

function timingReel4 (){
    while (spin[3].currentFrame.name != SlotFourResult){
        console.log("delayed");
        spin[3].next();
    }
    spin[3].stop();
    stopButtonAudio.play();
    var frame = spin[3].currentFrame.index;
     if(frame % 4 === 1){
       spin[3].next(3);
    }else if(frame % 4 === 2){
       spin[3].next(2);
    }else if(frame % 4 === 3){
       spin[3].next(1);
    }
}

function timingReel5 (){
    while (spin[4].currentFrame.name != SlotFiveResult){
        console.log("delayed");
        spin[4].next();
    }
    spin[4].stop();
    stopButtonAudio.play();
    var frame = spin[4].currentFrame.index;
     if(frame % 4 === 1){
       spin[4].next(3);
    }else if(frame % 4 === 2){
       spin[4].next(2);
    }else if(frame % 4 === 3){
       spin[4].next(1);
    }
    checkPrizes();
    canSpin = true;
    if(auto == true){
        spinButtonHandler();
    }
}

window.onload = function() {
        game = new Phaser.Game(700, 750, Phaser.AUTO, 'game', { preload: preload, create: create, render: render, onSpin1CompleteHandler: onSpin1CompleteHandler, onSpin1LoopHandler: onSpin1LoopHandler, onSpin1StartHandler: onSpin1StartHandler});
}

function preload(){
        this.progressText = this.add.text((this.game.width - this.PRELOADBAR_WIDTH) / 2, this.world.centerY - 60, '', {font: '28px Arial', fill: '#d9e021'});
        this.load.onFileComplete.add((function(){
            this.progressText.text = this.load.progress + '%';
        }).bind(this), this);
        this.load.atlasJSONHash('spinsheet1', 'basic-slot/images/spinanim1.png', 'basic-slot/images/spinanim1.json');
        this.load.atlasJSONHash('spinsheet2', 'basic-slot/images/spinanim2.png', 'basic-slot/images/spinanim2.json');
        this.load.atlasJSONHash('spinsheet3', 'basic-slot/images/spinanim3.png', 'basic-slot/images/spinanim3.json');
        this.load.image('background', 'basic-slot/images/background.png');
        this.load.spritesheet('arrow', 'basic-slot/images/arrow.png', 15, 15);
        this.load.spritesheet('spin-button', 'basic-slot/images/btn_spin.png', 152, 88);
        this.load.image('auto', 'basic-slot/images/btn_auto.png');
        this.load.image('hit-line', 'basic-slot/images/hit-line.png');
        this.load.image('header','../common/images/header.png');
        this.load.audio('spin-button-audio', 'basic-slot/audio/spin-button.ogg');
        this.load.audio('stop-button-audio', 'basic-slot/audio/stop-button.ogg');
        this.load.audio('game-theme-audio', 'basic-slot/audio/theme-tune.ogg');
        this.load.audio('win-audio', 'basic-slot/audio/tada.mp3');
}

function create(){
        this.stage.backgroundColor = '#fff';
        spinsheet[0] = this.add.sprite(70, 0, 'spinsheet1', 0);
        spinsheet[1] = this.add.sprite(190, 0, 'spinsheet2', 0);
        spinsheet[2] = this.add.sprite(310, 0, 'spinsheet3', 0);
        spinsheet[3] = this.add.sprite(430, 0, 'spinsheet1', 0);
        spinsheet[4] = this.add.sprite(550, 0, 'spinsheet2', 0);
        spin[0] = spinsheet[0].animations.add('spin');
        spin[0].onStart.add(onSpin1StartHandler, this);
        spin[0].onLoop.add(onSpin1LoopHandler, this);
        spin[1] = spinsheet[1].animations.add('spin');
        spin[2] = spinsheet[2].animations.add('spin');
        spin[3] = spinsheet[3].animations.add('spin');
        spin[4] = spinsheet[4].animations.add('spin');
        this.panel = this.add.sprite(0, -1, 'background');
        this.add.sprite(55, 450, 'arrow', 0);
        this.add.sprite(660, 450, 'arrow', 1);
        hitLine = this.add.sprite(115, 518, 'hit-line');
        header = this.game.add.sprite(this.game.world.centerX, 75, "header");
        header.scale.setTo(0.75,0.75);
        header.anchor.set(0.5);
        header.inputEnabled = true;
        header.input.pixelPerfectClick = true;
        header.input.pixelPerfectOver = true;
        hitLine.alpha = 0;
        spinButton = this.add.button(this.world.centerX, 640, 'spin-button', spinButtonHandler, this, 0, 0, 1, 0);
        autoButton = this.add.button(this.world.centerX + 100, 640, 'auto', autoButtonHandler, this, 0, 0, 1, 0);
        spinButton.anchor.setTo(0.5, 0);
        spinButton.input.useHandCursor = true;
        autoButton.input.useHandCursor = true;
        this.game.time.advancedTiming = true;
        spinButtonAudio = this.add.audio('spin-button-audio');
        stopButtonAudio = this.add.audio('stop-button-audio');
        winAudio = this.add.audio('win-audio');
        this.gameThemeAudio = this.add.audio('game-theme-audio', 1, true);
        this.gameThemeAudio.play('', 0, 1, true);
        prizeText = this.add.text(this.world.centerX, 120, 0, { font: "30px Arial", fill: "#fff", align: "center" });
        prizeText.text = "Last Prize: 0";
        prizeMoney = game.add.text(295, 60, "", { font: "15px Arial", fill: "#fff", align: "center" });
        prizeMoney.text = "000000";
        prizeMoney.anchor.setTo(0.5, 0);
        changeMoney();

        buy = game.add.text(433, 60, "", { font: "15px Arial", fill: "#fff", align: "center" });
        buy.inputEnabled = true;
        buy.text = "       ";
        buy.input.useHandCursor = true;
        buy.events.onInputDown.add(clickBuy, this);

        lobby = game.add.text(140, 60, "", { font: "15px Arial", fill: "#fff", align: "center" });
        lobby.inputEnabled = true;
        lobby.text = "          ";
        lobby.input.useHandCursor = true
        lobby.events.onInputDown.add(clickLobby, this);

        shareGame = game.add.text(500, 60, "", { font: "15px Arial", fill: "#fff", align: "center" });
        shareGame.inputEnabled = true;
        shareGame.text = "          ";
        shareGame.input.useHandCursor = true
        shareGame.events.onInputDown.add(clickShare, this);
    }

function clickBuy(){
  $( "#popupLogin" ).popup( "open");
}

function clickLobby(){
  console.log('lobby');
  window.location.href = "../index.html";
}

function clickShare(){
  console.log('share');
  var url = window.location.href;
  FB.ui({
    method: 'share',
    href: url,
  }, function(response){});
}

function checkPrizes(){
    if(prize > 0){
        console.log("Premio!!");
        winAudio.play();
        game.add.tween(spin[0].currentFrame).to( { alpha: 1 }, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);
        prizeText.text = "Last Prize: " + prize;
        prize = 0;
    }else{
        console.log("NO Premio");
    }
    changeMoney();
}

function spinButtonHandler(){
    if(canSpin == true){
        fireServerCall();
        canSpin = false;
        var randomResult = Math.floor((Math.random() * 2000) + 1);
        window.setTimeout(timingReel1, randomResult);
        window.setTimeout(timingReel2, randomResult + 1500);
        window.setTimeout(timingReel3, randomResult + 3000);
        window.setTimeout(timingReel4, randomResult + 4500);
        window.setTimeout(timingReel5, randomResult + 5500);
    }
}

function fireServerCall(){
        var projectId = 2001
        xhr.open('POST', "../backend/bet.php", true);
        xhr.addEventListener("readystatechange", processRequest, true);
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.send('id=39403393&token=Z9df0a7dgS1ej1Em37grfa24o8RL&user='+ userId +'&key=key&project='+ projectId);
        console.log('id=39403393&token=Z9df0a7dgS1ej1Em37grfa24o8RL&user='+ userId +'&key=key&project='+ projectId);
}

function processRequest() {
        if (xhr.readyState == 4 && xhr.status == 200) {
          console.log(xhr);
          var data = JSON.parse(xhr.response);
          if(data.status === 'false'){
              console.log(data.status);
              console.log(data);
              alert('Error conecting with server: Error '+ xhr.status);
              return;
          }
        spinButtonAudio.play();
        spin[0].play(30, true);
        spin[1].play(30, true);
        spin[2].play(30, true);
        spin[3].play(30, true);
        spin[4].play(30, true);
        SlotOneResult = data.reel1;
        SlotTwoResult = data.reel2;
        SlotThreeResult = data.reel3;
        SlotFourResult = data.reel4;
        SlotFiveResult = data.reel5;
        prize = data.prize;
        }else if (xhr.status != 200 && xhr.readyState == 4){
             console.log("Error conection with the server.");
             alert('Error conecting with server: Error '+ xhr.status +' We will reload the page trying to resolve this issue.');
             window.location.reload(false);
        }
}

function autoButtonHandler(){
    if (auto == false){
        auto = true;
    } else{
        auto =false;
    }
}

function changeMoney(){
        xhr_money.open('POST', "../backend/balance.php", true);
        xhr_money.addEventListener("readystatechange", processMoney, true);
        xhr_money.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr_money.send('user_id=' + userId);
        return userId;
}

function processMoney(){
  var data;
  if (xhr_money.readyState == 4 && xhr_money.status == 200) {
        data = JSON.parse(xhr_money.response);
        prizeMoney.text = data.balance;
  }
}

function onSpin1StartHandler(){}

function onSpin1LoopHandler(){}

function onSpin1CompleteHandler(){}

function render(){
}
